# If it looks stupid but it works, it ain't stupid

# Todo: add match/visit; annual season summary;
# attended matches by year

import xlrd
import sys

if len(sys.argv) >= 2:
    loc = sys.argv[1]
else:
    print("Please enter a parameter.")
    exit()

# open excel file
wb = xlrd.open_workbook(loc)
sheet = wb.sheet_by_index(0)

grounds = {}
grounds_with_visits = {}
number_of_grounds = int(-1)
countries = {}
teams = {}
teams_by_number = {}
clubs = {}
matches = 0
number_of_matches = {}
allmatches = []

# reads cells, splits teams and data by home and away
for i in range(sheet.nrows):
    type_ = sheet.cell_value(i, 0)
    date = sheet.cell_value(i, 1)
    land = sheet.cell_value(i, 3).split()[-1]
    stadium = sheet.cell_value(i, 4)
    game = sheet.cell_value(i, 5)
    if type_ == "Match":
        allmatches.append(game)
        team = game.split(" vs ")
        matches += 1

        # teams by matches
        if team[0] not in number_of_matches:
            number_of_matches[team[0]] = 1
        else:
            number_of_matches[team[0]] += 1

        if team[1] not in number_of_matches:
            number_of_matches[team[1]] = 1
        else:
            number_of_matches[team[1]] += 1

        # teams by grounds
        if team[0] not in clubs:
            clubs[team[0]] = {}
            clubs[team[0]]["stadiums"] = []
            clubs[team[0]]["stadiums"].append(stadium)
        else:
            if stadium not in clubs[team[0]]["stadiums"]:
                clubs[team[0]]["stadiums"].append(stadium)

        if team[1] not in clubs:
            clubs[team[1]] = {}
            clubs[team[1]]["stadiums"] = []
            clubs[team[1]]["stadiums"].append(stadium)
        else:
            if stadium not in clubs[team[1]]["stadiums"]:
                clubs[team[1]]["stadiums"].append(stadium)

        # teams by home and away games
        if team[0] in teams:
            teams[team[0]]["Home"] += 1
        else:
            teams[team[0]] = { "Home": 0, "Away": 0 }
            teams[team[0]]["Home"] += 1

        if team[1] in teams:
            teams[team[1]]["Away"] += 1
        else:
            teams[team[1]] = { "Home": 0, "Away": 0 }
            teams[team[1]]["Away"] += 1

        print(date)

    # creates a dict with grounds and each ground with country and visits
    if stadium in grounds:
        grounds[stadium]["visits"] += 1
    else:
        grounds[stadium] = { "visits": 1, "country": land}
        number_of_grounds += 1

grounds.pop("Ground")

# all grounds
for x in grounds:
    grounds_with_visits[x] = grounds[x]["visits"]

# grounds by country
for w in grounds:
    if grounds[w]["country"] in countries:
        countries[str(grounds[w]["country"])][w] = grounds[w]["visits"]
    else:
        countries[str(grounds[w]["country"])] = { str(w): grounds[w]["visits"] }

# number of countries
ncountries = len(countries)

# total times seen team
for h in teams:
    teams_by_number[h] = teams[h]["Home"] + teams[h]["Away"]

# menu
while True:
    print("""
1. Grounds visited     4. All matches
2. Match visits        5. Most seen teams
3. Countries visited   6. Stats by team""")
    num = input("> ")
    while not (num == "1" or num == "2" or num == "3" or num == "4" or num == "5" or num == "6"):
        print("Please type a number.")
        num = input("> ")

    # grounds visited
    if num == "1":
        inp = input("All time or by country? (all or country): ")
        while not (inp == "all" or inp == "country"):
            print("Please type \"all\" or \"country\".")
            inp = input("> ")

        if inp == "all":
            print("\n")
            print("You have visited " + str(number_of_grounds) + " grounds.")
            grounds_with_visits = {k: v for k, v in reversed(sorted(grounds_with_visits.items(), key=lambda x: x[1]))}
            for xyz in grounds_with_visits:
                print(xyz + " " * (50 - len(xyz)) + str(grounds_with_visits[xyz]))

        if inp == "country":
            print("\n")

            for y in countries:
                sor = {k: v for k, v in reversed(sorted(countries[str(y)].items(), key=lambda x: x[1]))}
                print("\n")
                print("You have visited " + str(len(countries[y])) + " grounds in " + str(y) + ": ")
                for xyz in sor:
                    print(xyz + " " * (50 - len(xyz)) + str(sor[xyz]))

    # match visits
    if num == "2":
        print("\nYou have attended " + str(matches) + " matches.")

    # countries visited
    if num == "3":
        print("\n")
        print("You have visited " + str(ncountries) + " countries.")

    # print all matches
    if num == "4":
        print("\nAll attended matches: ")
        for x in allmatches:
            print(x)

    # most seen teams
    if num == "5":
        sor = {k: v for k, v in reversed(sorted(teams_by_number.items(), key=lambda x: x[1]))}
        print("\n")
        for xyz in sor:
            print(xyz + " " * (30 - len(xyz)) + str(sor[xyz]))

    # stats by team
    if num == "6":
        print("Type the name of the team.")
        tea = input("> ")
        while not (tea in teams):
            print("Please enter a valid team.")
            tea = input(">")

        print("\n")
        print("You have seen " + str(teams[tea]["Home"]) + " home and " + str(teams[tea]["Away"]) + " away games with " + tea + ", in a total of " + str(number_of_matches[tea]) + " matches in " + str(len(clubs[tea]["stadiums"])) + " grounds.")